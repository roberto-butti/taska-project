<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailylogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dailylogs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('people_id');
            $table->date('date');
            $table->string('title');
            $table->string('plan');
            $table->string('done');
            $table->string('blockers');
            $table->timestamps();

            $table->foreign('people_id')->references('id')->on('people')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dailylogs');
    }
}
