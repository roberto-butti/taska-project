<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href='{{ backpack_url('issue') }}'><i class='fa fa-tag'></i> <span>Issues</span></a></li>
<li><a href='{{ backpack_url('people') }}'><i class='fa fa-user'></i> <span>People</span></a></li>
<li><a href='{{ backpack_url('dailylog') }}'><i class='fa fa-calendar'></i> <span>Daily Log</span></a></li>